@extends('layouts.app')

@section('content')
    <div class="text-center">
        <h1>Agrega productos</h1>
    </div>

    <div class="container mt-5">
        <div class="row">
            <div class="col-md-6 col-xl-4 mx-auto">
                <form action="{{route('products.store')}}" method="post">
                    @csrf
                    <div class="form-group">
                        <input type="text" name="title" placeholder="Titulo" class="form-control" />
                    </div>

                    <div class="form-group">
                        <input type="text" name="description" placeholder="Descripcion" class="form-control" />
                    </div>

                    <div class="form-group">
                        <input type="text" name="pricing" placeholder="Precio" class="form-control" />
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block">
                            Guardar
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
