<form action="{{ route('products.destroy', $products->id) }}" method="post" class="d-inline-block">
    {{ method_field('DELETE') }}
    @csrf
    <button type="submit" class="btn btn-danger" onclick="return confirm('Quieres eliminarlo?')">
        Eliminar
    </button>
</form>
