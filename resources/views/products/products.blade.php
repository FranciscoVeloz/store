@extends('layouts.app')

@section('content')
    <div class="text-center">
        <h1>Productos</h1>
    </div>

    <div class="container mt-5">
        <div class="row">
            <div class="col-xl-12 text-center">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th>ID</th>
                            <th>Titulo</th>
                            <th>Descripcion</th>
                            <th>Precio</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($product as $products)
                            <tr>
                                <td>{{$products->id}}</td>
                                <td>{{$products->title}}</td>
                                <td>{{$products->description}}</td>
                                <td>{{$products->pricing}}</td>
                                <td>
                                    <a href="{{url('/products/'.$products->id.'/edit')}}" class="btn btn-info">Editar</a>
                                    @include('products.delete', ['products'=>$products])
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <a href="{{url('/products/create')}}" class="btn btn-primary">Agregar</a>
            </div>
        </div>
    </div>
@endsection
