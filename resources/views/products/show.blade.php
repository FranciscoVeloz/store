@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-xl-4 mx-auto text-center">
            <div class="card">
                <div class="card-header">
                    <h1>{{$producto->title}}</h1>
                </div>
                <div class="card-body">
                    <h2>{{$producto->description}}</h2>
                    <h2>{{$producto->pricing}}</h2>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
